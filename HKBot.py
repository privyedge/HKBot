# stdlib
import argparse
import collections
import dbm
import itertools
import json
import pprint
import random
try: random = random.SystemRandom()
except: pass
import re
import shlex
import sys
import time

# external deps
from matrix_client_core.ratelimit import RateLimit
import matrix_client_core as botcore
import matrix_client_core.nocurses as nocurses
import matrix_client_core.notifier as notifier
from matrix_client_core.util import getchain

# in-tree deps
import obliterator
import taskscheduler


minutes = 60

def trace(*args):
	notifier.notify(__file__, 'toys.HKBot.trace', args)

def format_time_interval(seconds, fmt_seconds="{:.3f} seconds"):
	units = (
			(fmt_seconds, 60),
			("{} minutes and ", 60),
			("{} hours, ", 24),
			("{} days, ", 365),
			("{} years, ", None),
	)


class NotificationPrinter(notifier.NotificationListener):
	@staticmethod
	def on_mcc_ratelimit_ok(service, event, data):
		s_ok = ("Not OK:", "OK:")[data[0]]
		print(s_ok, data[1], data[2])

	@staticmethod
	def on_mcc_ratelimit_consume(service, event, data):
		print("Consume!", data)

	@staticmethod
	def on_mcc_ratelimit_replenish(service, event, data):
		print("Replenish!", data)

	@staticmethod
	def on_toys_HKBot_trace(service, event, data):
		print(event + ":", *data)

	@staticmethod
	def on_HKBot_obliterator_trace(service, event, data):
		print(event + ":", *data)

	@staticmethod
	def on_toys_HKBot_send_notice(service, event, data):
		nocurses.print("Sending message to {}: {}".format(*data), fg='red')


class Rule:
	NEXT_GROUP_ID = 0

	_key = collections.namedtuple("rule_key", "roomid user ruleid")
	_pattern = collections.namedtuple("rule_pattern", "roomid eventtype eventcontenttype body formattedbody user userclass")

	def __init__(self, rulename, limit, consequences, group=None, userclass=None, roomid=None, eventtype=None, eventcontenttype=None, body=None, formattedbody=None, user=None):
		self.name = rulename

		if isinstance(consequences, type) and issubclass(consequences, Consequence):
			self.consequences = frozenset([consequences])
		else:
			self.consequences = frozenset(consequences)

		trace("Rule.consequences set to: {!r}".format(self.consequences))

		self.pattern = self._pattern(*(re.compile(x) if isinstance(x, str) else x
			for x in (roomid, eventtype, eventcontenttype, body, formattedbody, user, userclass)))
		self.limit = limit

		# Automatically assign an unique group ID to this rule, to effect "not grouping".
		# Otherwise, all rules would end up grouped under 'None'.
		if group is None:	self.group = self._make_unique_group_id()
		else:			self.group = group

	@classmethod
	def _make_unique_group_id(klass):
		b = klass.NEXT_GROUP_ID
		klass.NEXT_GROUP_ID += 1
		return b

	def getkey(self, event):
		""" Key to file this rule infraction under """

		roomid = getchain(event, 'room_id', default="")
		sender = getchain(event, 'sender')
		return self._key(roomid, sender, self.group)

	def match(self, event, userclass=None):
		""" Match event against the rule

		returns a hit counter key if it matches,
		None otherwise """

		roomid = getchain(event, 'room_id', default="")
		eventtype = getchain(event, 'type', default="")
		eventcontenttype = getchain(event, 'content', 'msgtype', default="")
		body = getchain(event, 'content', 'body', default="")
		formattedbody = getchain(event, 'content', 'formatted_body', default="")
		sender = getchain(event, 'sender')

		data = self._pattern(roomid, eventtype, eventcontenttype, body, formattedbody, sender, userclass)

		for pattern, text in zip(self.pattern, data):
			if pattern is None:
				continue
			if not pattern.search(text):
				return None

		return self.getkey(event)

	def __repr__(self):
		return "Rule {!r}:<{!r} limit:{} group:{!r}>".format(self.name, self.pattern, self.limit, self.group)


class Infraction:
	def __init__(self, event, rules):
		self.event = event
		self.rules = rules

	def get_consequences(self):
		ret = frozenset(itertools.chain.from_iterable(rule.consequences for rule in self.rules))
		trace("get_consequences: {!r}".format(ret))
		return ret

	def get_rule_names(self):
		return ", ".join(rule.name for rule in self.rules)

	def __bool__(self):
		return bool(self.rules)


class Consequence:
	priority = 1

	@classmethod
	def apply(klass, sdkclient, infraction, **kwargs):
		try:
			roomid = getchain(infraction.event, 'room_id')
			sender = getchain(infraction.event, 'sender')
			reason = infraction.get_rule_names()
			trace("{!r}.apply: {!r} {!r} {!r}".format(klass, roomid, sender, reason))
			klass._apply(sdkclient, infraction, roomid, sender, reason, **kwargs)
		except NotImplementedError as e:
			raise
		except Exception as e:
			sys.excepthook(*sys.exc_info())

	@staticmethod
	def _apply(sdkclient, infraction, roomid, sender, reason, **kwargs):
		raise NotImplementedError("Subclasses should override this method")


class ConsequenceKick(Consequence):
	priority = 1

	@staticmethod
	def _apply(sdkclient, infraction, roomid, sender, reason, **kwargs):
		sdkclient.api.kick_user(roomid, sender, reason)


class ConsequenceBan(Consequence):
	priority = 0

	@staticmethod
	def _apply(sdkclient, infraction, roomid, sender, reason, **kwargs):
		sdkclient.api.ban_user(roomid, sender, reason)


class ConsequenceRedact(Consequence):
	priority = 2

	@staticmethod
	def _apply(sdkclient, infraction, roomid, sender, reason, **kwargs):
		sdkclient.api.redact_event(roomid, infraction.event['event_id'], reason)


class ConsequenceRedactAll(Consequence):
	priority = 3

	@staticmethod
	def _apply(sdkclient, infraction, roomid, sender, reason, **kwargs):
		o = obliterator.Obliterator(kwargs['scheduler'], sdkclient, roomid, sender, reason=reason)
		o.start()


class RecentLimitDB:
	def __init__(self, expire_seconds=120):
		self.expire_seconds = expire_seconds
		self.creation_time = 0
		self.database = None
		self.rules = []
		self.debug_file = open("junk_hkbot/recent.dbg", "w", encoding='UTF-8')

	def add_rule(self, rule):
		self.rules.append(rule)

	def is_limit_exceeded(self, event):
		self._run_maintenance()
		rules = []

		for rule in self.rules:
			key = rule.match(event)
			#trace("  Limit: {!r} {!r} {!r} {!r}".format(rule, key, self.database.get(key), rule.limit))
			if key is not None:
				self.database[key] += 1
				if self.database[key] > rule.limit:
					trace("Limit: {!r} {!r} {!r} {!r}".format(rule, key, self.database.get(key), rule.limit))
					rules.append(rule)

		return Infraction(event, rules)

	def _run_maintenance(self):
		now = time.time()
		if self.creation_time + self.expire_seconds < now:
			trace("Database was:")
			trace(pprint.pformat(self.database))
			pprint.pprint(self.database, stream=self.debug_file)
			print(file=self.debug_file, flush=True)

			self.database = collections.defaultdict(int)
			self.creation_time = now


class IdleTrackingClient(botcore.NoSyncMatrixClient):
	def __init__(self, *args, **kwargs):
		self.debug_file = open("junk_hkbot/debug-hkbot.dbg", "w", encoding='UTF-8')
		self.state = collections.defaultdict(lambda: collections.defaultdict(dict))
		botcore.NoSyncMatrixClient.__init__(self, *args, **kwargs)

	def get_rooms(self):
		rooms = {}
		for roomid, state in self.state.items():
			room = self._mkroom(roomid)
			room.canonical_alias = getchain(state, 'm.room.canonical_alias', '', 'content', 'alias')
			room.aliases = set()
			if 'm.room.aliases' in state:
				for event in state['m.room.aliases'].values():
					room.aliases.update(getchain(event, 'content', 'aliases', default=()))
			rooms[roomid] = room
		return rooms

	def _sync(self, timeout_ms=150000):
		self.sync_attempted = True
		self.sync_args = [timeout_ms]
		self.sync_kwargs = {}
		if not self.sync_enabled: return

		response = self.api.sync(self.sync_token, timeout_ms, filter=self.sync_filter)
		self.sync_token = response['next_batch']

		print(json.dumps(response), file=self.debug_file)
		print(file=self.debug_file, flush=True)
		for roomid, data in response['rooms']['join'].items():
			for event in itertools.chain(data['state']['events'], data['timeline']['events']):
				if 'state_key' in event:
					self.state[roomid][event['type']][event['state_key']] = event
		self._sync2(response)

	def _sync2(self, response):
		""" This is a near-copy of matrix_client.client.MatrixClient._sync

		At the moment, there is no other way to intercept the raw
		response whilst keeping the sdk's functionality intact. """

		self.sync_token = response["next_batch"]

		for presence_update in response['presence']['events']:
			for callback in self.presence_listeners.values():
				callback(presence_update)

		for room_id, invite_room in response['rooms']['invite'].items():
			for listener in self.invite_listeners:
				listener(room_id, invite_room['invite_state'])

		for room_id, left_room in response['rooms']['leave'].items():
			for listener in self.left_listeners:
				listener(room_id, left_room)
			if room_id in self.rooms:
				del self.rooms[room_id]

		for room_id, sync_room in response['rooms']['join'].items():
			if room_id not in self.rooms:
				self._mkroom(room_id)
			room = self.rooms[room_id]
			# TODO: the rest of this for loop should be in room object method
			room.prev_batch = sync_room["timeline"]["prev_batch"]

			for event in sync_room["state"]["events"]:
				event['room_id'] = room_id
				room._process_state_event(event)

			for event in sync_room["timeline"]["events"]:
				event['room_id'] = room_id
				room._put_event(event)

				# TODO: global listeners can still exist but work by each
				# room.listeners[uuid] having reference to global listener

				# Dispatch for client (global) listeners
				for listener in self.listeners:
					if (
						listener['event_type'] is None or
						listener['event_type'] == event['type']
					):
						listener['callback'](event)

			for event in sync_room['ephemeral']['events']:
				event['room_id'] = room_id
				room._put_ephemeral_event(event)

				for listener in self.ephemeral_listeners:
					if (
						listener['event_type'] is None or
						listener['event_type'] == event['type']
					):
						listener['callback'](event)


class HKBot(botcore.MXClient):
	def __init__(self, *args, **kwargs):
		self.limit = RecentLimitDB()
		self.scheduler = taskscheduler.Scheduler()
		if not 'sdkclient_factory' in kwargs:
			kwargs['sdkclient_factory'] = IdleTrackingClient
		botcore.MXClient.__init__(self, *args, **kwargs)

	@botcore.wrap_exception
	def on_m_room_message(self, event):
		self.last_event = event
		nocurses.print(time.strftime("%c", time.gmtime()), fg='cyan')
		#nocurses.print(pprint.pformat(event), fg='yellow')

		sender = event['sender']
		eventid = event['event_id']
		roomid = event['room_id']
		inmsg = event['content']['body']
		pl = getchain(self.sdkclient.state, roomid, 'm.room.power_levels', '', 'content', 'users', sender)

		if not isinstance(pl, int) or pl <= 0:
			#self._print_user(roomid, sender)
			infraction = self.limit.is_limit_exceeded(event)
			if infraction:
				nocurses.print(pprint.pformat(event), fg='yellow')
				nocurses.print("Rules broken:", infraction.get_rule_names(), fg='magenta')
				nocurses.print("Consequences:", repr(infraction.get_consequences()), fg='magenta')

				for consequence in infraction.get_consequences():
					self.scheduler.add(consequence.apply, (self.sdkclient, infraction),
						{'scheduler': self.scheduler}, group=roomid, priority=consequence.priority)

		if not event['content']['msgtype'] == "m.text": return

		if not isinstance(pl, int) or pl <= 50:
			if not self.ratelimit.ok(): return

		reply = None

		my_mxid = self.account.mxid
		my_displayname = getchain(self.sdkclient.state, roomid, 'm.room.member', my_mxid, 'content', 'displayname')
		if my_displayname is None: return

		for prefix in (my_mxid + ": ", my_displayname + ": ", "{} ({}): ".format(my_displayname, my_mxid)):
			if inmsg.startswith(prefix):
				reply = self.handle_command(event, inmsg[len(prefix):])
				break

		# This one is critical enough to warrant a bang command
		if reply is None and inmsg.lower().strip() == "!stop":
			reply = self.cmd_stop(event)

		if reply:
			notifier.notify(__file__, 'toys.HKBot.send_notice', (roomid, reply))
			self.ratelimit.consume()
			self.sendmsg(roomid, reply)

	def handle_command(self, event, command):
		try:
			words = shlex.split(command)
		except ValueError:
			print("Warning: parse error for", repr(command))
			return None

		if len(words) == 0: return None

		cmd = words[0]

		m = getattr(self, 'cmd_' + cmd, None)
		if callable(m):
			try:
				return m(event, *words[1:])
			except TypeError:
				return "Incorrect number of arguments for {0!r}. Try \"help {0}\".".format(cmd)
		else:
			return "Unrecognized command: {!r}. Try \"help\".".format(cmd)

	def cmd_help(self, event, cmd=None):
		""" Get short help on commands """

		cmds = tuple(x[4:] for x in dir(self) if x.startswith('cmd_'))
		if cmd is None:
			return("Available commands: {}".format(", ".join(cmds)))

		if cmd not in cmds:
			return("No help available for {!r}".format(cmd))

		m = getattr(self, "cmd_" + cmd)
		return "help[{}]: {}".format(cmd, "\n".join(x.strip() for x in getattr(m, '__doc__', "").splitlines()))

	def cmd_obliterate(self, event, target, *reason):
		""" obliterate <user> [reason]

		REDACT ALL MESSAGES FROM A USER. THIS IS DANGEROUS.
		Use the "stop" command to cancel a running operation. """

		roomid = getchain(event, 'room_id')
		sender = getchain(event, 'sender')
		pl = getchain(self.sdkclient.state, roomid, 'm.room.power_levels', '', 'content', 'users', sender)

		if not isinstance(pl, int) or pl < 90:
			return "Access denied."

		valid_users = getchain(self.sdkclient.state, roomid, 'm.room.member', default={})
		if target not in valid_users:
			return "User {!r} is not, and was never in the room.".format(target)

		o = obliterator.Obliterator(self.scheduler, self.sdkclient, roomid, target, reason=" ".join(reason))
		o.start()

	def cmd_stop(self, event):
		""" Stop all running tasks for this room immediately. """
		roomid = getchain(event, 'room_id')
		sender = getchain(event, 'sender')
		pl = getchain(self.sdkclient.state, roomid, 'm.room.power_levels', '', 'content', 'users', sender)

		if not isinstance(pl, int) or pl < 50:
			return "Access denied."

		self.scheduler.cancel(roomid)
		return "Cleared the queue."

	def set_database(self, filename):
		self.dbfilename = filename
		return self

	def _print_user(self, roomid, user):
		ud = getchain(self.sdkclient.state, roomid, 'm.room.member', user)
		if ud is None:
			print("No such user!")
			return True
		now = time.time()
		pl = getchain(self.sdkclient.state, roomid, 'm.room.power_levels', '', 'content', 'users', user)
		uage = getchain(ud, 'origin_server_ts')
		if uage is not None: uage = now - uage // 1000

		print("User:", user)
		print("    Power level  :", pl)
		print("    Membership   :", getchain(ud, 'content', 'membership'))
		print("    Display name :", getchain(ud, 'content', 'displayname'))
		print("    Avatar URL   :", getchain(ud, 'content', 'avatar_url'))
		print("    Untrusted Age:", format_time_interval(uage) if uage else repr(uage))

	def repl_dumpstate(self, txt):
		""" Dump room state """
		print(json.dumps(self.sdkclient.state), file=self.sdkclient.debug_file)
		print(file=self.sdkclient.debug_file, flush=True)
		print("State dumped.")
		return True

	def repl_whois(self, txt):
		""" Show user info, or without arguments, list users """
		args = txt.split()

		room = self.foreground_room
		user = None

		if len(args) < 2:
			pass
		elif len(args) == 2:
			if room is None: room = self.rooms.get_room(args[1])
			else: user = args[1]
		elif len(args) == 3:
			room = self.rooms.get_room(args[1])
			user = args[2]
		else:
			print("Too many arguments!")
			return True

		if room is None:
			print("No room specified!")
			return True

		if user is None:
			for u, ud in sorted(getchain(self.sdkclient.state, room.room_id, 'm.room.member', default={}).items()):
				membership = getchain(ud, 'content', 'membership')
				fg = { 'join': 'green', 'leave': 'red', 'invite': 'blue', 'ban': 'magenta' }.get(membership)
				nocurses.print(u, end=' ', fg=fg)
			print()
		else:
			self._print_user(room.room_id, user)

		return True

	def repl_obliterate(self, txt):
		""" REDACT ALL OF A USERS MESSAGES, UP UNTIL THEIR MOST RECENT JOIN EVENT """

		args = txt.split()
		room = self.foreground_room

		if len(args) < 2:
			print("Not enough arguments!")
			return True
		elif len(args) == 2:
			user = args[1]
		elif len(args) == 3:
			room = self.rooms.get_room(args[1])
			user = args[2]
		else:
			print("Too many arguments!")
			return True

		if room is None:
			print("No room specified!")
			return True

		valid_users = getchain(self.sdkclient.state, room.room_id, 'm.room.member', default={})
		if user not in valid_users:
			print("User {!r} is not, and was never in the room.".format(user))
			return True

		o = obliterator.Obliterator(self.scheduler, self.sdkclient, room.room_id, user)
		o.start()

		return True

	def connect(self):
		self.ratelimit = RateLimit(0.7, 2.0)
		self.ratelimit.start_replenish_thread(120)

		self.sync_filter = '''{
			"presence": { "types": [ "" ] },
			"room": {
				"ephemeral": { "types": [ "" ] },
				"state": {
					"types": [
						"m.room.canonical_alias",
						"m.room.aliases",
						"m.room.member",
						"m.room.name",
						"m.room.power_levels",
						"m.room.topic"
					]
				},
				"timeline": {
					"types": [ "*" ],
					"limit": 3
				}
			}
		}'''

		self.is_bot = True
		self.login()
		self.first_sync()
		self.sdkclient.add_listener(self.on_m_room_message, 'm.room.message')
		self.start_send_thread(self.sdkclient.api.send_notice)
		self.hook()

	def run_forever(self):
		print("Ready.")
		self.repl()


def parse_args():
	parser = argparse.ArgumentParser(description="HKBot monitors rooms for problem messages and users, and removes them.")
	parser.add_argument("-d", "--debug", action='store_true', default=False, help="Enable debug messages.")
	parser.add_argument("--database", default="database-hkbot", help="Filename of the database, without extension.")
	options = parser.parse_args()
	return options

if __name__ == '__main__':
	try: import site_config
	except ImportError: pass

	opts = parse_args()
	if opts.debug: np = NotificationPrinter()

	bot = HKBot('account-hkbot.json').set_database(opts.database)

	def add_rule(*args, **kwargs):
		rule = Rule(*args, **kwargs)
		bot.limit.add_rule(rule)

	add_rule("Image", 3, ConsequenceBan, eventtype="^m.image$")
	add_rule("Any", 10, ConsequenceBan)
	add_rule("Spam1", 0, (ConsequenceRedact, ConsequenceBan), group="spam", body="mysuperspammydomain\\.com")
	add_rule("Test1", 1, ConsequenceKick, body="^kickme$")
	add_rule("Obliterate Test", 2, ConsequenceRedactAll, body="^Please redact all of my messages\\.$", group="obliterate_test")

	bot.connect()
	bot.run_forever()

#!/bin/sh

PYTHONFLAGS=

while [ "$1" ]; do case "$1" in
	-i)	PYTHONFLAGS="$PYTHONFLAGS $1"
		shift
		;;
	-W)	PYTHONFLAGS="$PYTHONFLAGS $1 $2"
		shift 2
		;;
	--)	shift
		break
		;;
	*)	break
		;;
esac; done

PYTHONPATH=.:../matrix-client-core:../urllib-requests-adapter:../matrix-python-sdk python3 $PYTHONFLAGS HKBot.py "$@"
